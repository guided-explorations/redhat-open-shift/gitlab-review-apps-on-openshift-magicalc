import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Calc from '../components/calc'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import {
  Link
} from "react-router-dom";

export default function Homepage(props) {
  return (
    <>
      <div className="heroBg w-100">

        <div className="d-flex align-items-center justify-content-center my-md-5">
          <div className="d-flex justify-content-center">
              <Col xs={10} md={12} className="d-block my-md-5 text-center bg-dark">
                <h1 className="display-4 font-weight-bold mt-5">MagiCALC</h1>
                <h4 className="my-4">A totally over-engineered very basic calculator</h4>
                  <Calc />
              </Col>
          </div>
        </div>
      </div>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </>
  )
}
